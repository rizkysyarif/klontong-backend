# Klontong Backend
 
## Table of contents

> * [Klontong Backend](#Klontong-backend)
>   * [Table of contents](#table-of-contents)
>   * [Installation](#installation)
>     * [Clone](#clone)
>     * [Change Branch](#change-branch)
>     * [Install Depedencies](#install-depedencies)
>     * [Edit Environment](#edit-environment)
>     * [Run Seeder](#run-seeder)
>     * [Run server development](#run-server-development)
>   * [Credentials](#credentials)
>   * [License](#license)
 
## Installation

### Clone
```
$ git clone https://gitlab.com/rizkysyarif/klontong-backend.git
$ cd klontong-backend
```

### Change Branch
```
$ git checkout master
```

### Install Depedencies
```
npm install
```

### Edit Environment
```
1. Rename .env.example to .env
2. Enter the value according to the available environment
```

### Run Seeder
```
npm run seeder:all
```

### Run server development
```
npm start
```

## Credentials
```
Username: admin
Password: admin
```

## License
MIT

